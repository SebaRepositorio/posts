<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $posts = Post::all();
        return view('posts', ['posts' => $posts]);
    }

    public function filter(Request $request) {
        $search = $request->get("search");
        
        $posts = Post::where('title', 'like', '%'.$search.'%')
            ->orWhere('description', 'like', '%'.$search.'%')
            ->get();
        
        return response()->json($posts);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    
    private function validateFormData($data, $id = null) {
        $rules = [
            'title' => 'required|min:4|max:100',
            'description' => 'required|min:10',
            'image' => 'mimes:jpeg,bmp,png,jpg,svg'
        ];

        if (!$id) { // Se valida un nuevo post
            $rules['slug'] = 'unique:posts,slug';
        }
        else { // Se valida un post existente
            $rules['slug'] = 'unique:posts,slug,'.$id;
        }
         
        return Validator::make($data, $rules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->except('_token');
        $data["slug"] = $this->createSlug($data["title"]);

        $validation = $this->validateFormData($data);
        if ($validation->fails())
            return response()->json($validation->errors(), 422);

        \DB::beginTransaction();
        try {
            if ($request->hasFile('image'))
                $data["image"] = $request->file('image')->store('public/images/posts');            
            
            $post = Post::create($data);                        
            \DB::commit();
            return response()->json($post);
        }
        catch (\Exception $error) {
            \DB::rollback();
            return response()->json($error->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug) {        
        $post = Post::where("slug", $slug)->first();
        if (!$post) return redirect('posts');
        
        return view('post', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    private function createSlug($title) {
        $slug = "";
        if (!empty($title)) {
            $slug = str_replace(" ", "-", $title);
        }
        
        return $slug;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post) {
        $data = $request->except('_token');        
        $data["slug"] = $this->createSlug($data["title"]);

        $validation = $this->validateFormData($data, $post->id);
        if ($validation->fails())
            return response()->json($validation->errors(), 422);

        \DB::beginTransaction();
        try {
            if ($request->hasFile('image')) {
                $data["image"] = $request->file('image')->store('public/images/posts');
                if (!empty($post->image)) \Storage::delete($post->image);                
            }
            
            $post->update($data);            
            \DB::commit();
            return response()->json($post);
        }
        catch (\Exception $error) {
            \DB::rollback();
            return response()->json($error->getMessage(), 500);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post) {           
        \DB::beginTransaction();
        try {
            if (!empty($post->image)) \Storage::delete($post->image);            
            $post->delete();
            \DB::commit();
            return response()->json($post);
        }
        catch (\Exception $error) {
            \DB::rollback();
            return response()->json($error->getMessage(), 500);
        }
    }
}

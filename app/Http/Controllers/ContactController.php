<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller {
    
    public function index() {        
        return view('contacts.index');
    }

    public function send_email(Request $request) {       
        $request->validate([
            'email' => 'required|email',
            'title' => 'required',
            'body' => 'required',
        ]);

        $data = $request->except('_token');
        //dump($data['email']); die;
        \Mail::to($data['email'])->send(new \App\Mail\SendMail($data));

        $msg = 'Email enviado correctamente a la direccion ingresada';
        return redirect('/contacts')->with('success', $msg)->withInput();
    }

}

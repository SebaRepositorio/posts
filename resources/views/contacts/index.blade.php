@extends('layout.app')

@section('title', 'Contact form')

@section('content')
<p class="h1">Formulario de contacto <small class="text-muted">Enviar email</small></p>

<div class="row">
    <div class="col-md-3"></div>

    <div class="col-md-6">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Error</strong> Hubo problemas en el envio.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        <div class="card text-dark bg-light mb-3 mt-5">
            <div class="card-header" style="text-align: center;">Datos de contacto</div>
            <div class="card-body">
                <form id="contact_form" action="/contacts/send_email" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="email_input" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email_input" name="email" value="{{ old('email') }}" placeholder="Ingrese un email" maxlength="100" required>
                    </div>
                    <div class="mb-3">
                        <label for="title_input" class="form-label">Titulo</label>
                        <input type="text" class="form-control" id="title_input" name="title" value="{{ old('title') }}" placeholder="Ingrese un titulo" minlength="4" maxlength="100" required>
                    </div>
                    <div class="mb-3">
                        <label for="body_input" class="form-label">Cuerpo</label>
                        <textarea name="body" class="form-control" id="body_input" rows="3" placeholder="Ingrese un cuerpo" minlength="10" maxlength="255" required>{{ old('body') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>                
            </div>
        </div>        
    </div>

    <div class="col-md-3"></div>
</div>

@endsection
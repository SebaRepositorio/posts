@extends('layout.app')

@section('title', 'Detail view')

@section('content')
<div class="row">
    <p class="h1">Informacion de post</p>

    <?php
    $src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLNfiNFRr_I6hBXektHKEvrXV4yr9KQ-K4C1MXARnB0uwHt2xkrxOWXu8Tb1fiwik2Jzg&usqp=CAU";
    if ($post->image) $src = Storage::url($post->image);
    ?>
   
    <div class="col-md-2" style="text-align: center;">
        <div class="btn-group-vertical">
            <button type="button" class="btn btn-outline-warning" id="edit_btn" data-post="<?= htmlspecialchars(json_encode($post)) ?>">Editar</button>
            <button type="button" class="btn btn-outline-danger" id="delete_btn" data-post_id="{{ $post->id }}">Eliminar</button>
            <a href="/posts" class="btn btn-outline-secondary" role="button">Volver</a>        
        </div>
    </div>

    <div class="col-md-8">    
        <div class="card text-dark bg-light mb-3">            
            <img src="{{ $src }}" class="card-img-top mx-auto d-block" alt="..." style="max-width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{ $post->title }}</h5>
                <p class="card-text">{{ $post->description }}</p>                                
                <p class="card-text"><small class="text-muted">{{ $post->updated_at->format('d/m/Y H:i') }}</small></p>
            </div>
        </div>        
    </div>
    <div class="col-md-2"></div>
</div>

@include('post_modal')
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('js/posts.js') }}"></script>
@endsection
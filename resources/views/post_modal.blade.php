<!-- Modal -->
<div class="modal fade" id="postModal" tabindex="-1" aria-labelledby="postModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="postModalLabel">Crear Post</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>


            <form method="POST" action="/posts">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="title_input" class="form-label">Titulo</label>
                        <input type="text" class="form-control" id="title_input" name="title" placeholder="Ingrese un titulo" minlength="4" maxlength="100" required>
                    </div>
                    <div class="mb-3">
                        <label for="description_input" class="form-label">Descripción</label>
                        <textarea name="description" class="form-control" id="description_input" rows="3" placeholder="Ingrese una descripción" minlength="10" required></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="image_input" class="form-label">Imagen</label>
                        <input class="form-control" type="file" id="image_input" name="image" accept="image/*">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
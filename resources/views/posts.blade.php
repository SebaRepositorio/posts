@extends('layout.app')

@section('title', 'List & Creation')

@section('content')
<p class="h1">Posts <small class="text-muted">Listado y creacion</small></p>

<div class="row mt-3">   
    <div class="col-md-2 mb-3" style="text-align: left;">
        <button type="button" class="btn btn-outline-primary" id="new_post_btn">Nuevo</button>
    </div>

    <div class="col-md-6"></div>

    <div class="col-md-4">      
        <form id="filter_posts_form">
            <div class="row">
                <div class="col-12 col-md-9 mb-3">
                    <input type="text" class="form-control" name="search" placeholder="Ingrese parte del titulo o descripcion">
                </div>
                <div class="col-12 col-md-3">
                    <button type="submit" class="btn btn-outline-secondary">Buscar</button>
                </div>
            </div>                   
        </form>
    </div>
</div>

<div class="row">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Titulo</th>
                <th scope="col">Fecha de creación</th>
                <th scope="col">Descripcion </th>
                <th scope="col">Accion</th>
            </tr>
        </thead>

        <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->created_at->format('d/m/Y H:i') }}</td>
                <td>{{ substr($post->description, 0, 20).' ...' }}</td>
                <td>
                    <a href="/posts/{{ $post->slug }}" class="btn btn-outline-secondary" role="button">Ver</a>
                    <button type="button" class="btn btn-outline-danger delete_btn" data-post_id="{{ $post->id }}">Borrar</button>                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


@include('post_modal')
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/posts.js') }}"></script>
@endsection
<html>

<head>
    <title>Syloper Posts - @yield('title')</title>

    @include('layout.head')

    @yield('head')
</head>

<body>
    @include('layout.navbar')

    <div class="container">
        @yield('content')
    </div>

    @include('layout.scripts')
</body>

</html>
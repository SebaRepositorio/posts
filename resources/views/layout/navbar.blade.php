<div aria-live="polite" aria-atomic="true" class="position-relative" style="z-index: 1060;">
    <div class="toast-container position-absolute top-0 end-0 p-3"></div>
</div>


<?php
$inPostsView = \Request::path() == "posts";
$inContactView = \Request::path() == "contacts";
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin-bottom: 2.5rem;">
    <div class="container-md">
        <a class="navbar-brand" href="/posts">Challenge</a>
    </div>

    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{ $inPostsView ? 'active' : '' }}" {{ $inPostsView ? 'aria-current="page"' : '' }} href="/posts">Posts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $inContactView ? 'active' : '' }}" {{ $inContactView ? 'aria-current="page"' : '' }} href="/contacts">Contacto</a>
            </li>
        </ul>
    </div>
</nav>
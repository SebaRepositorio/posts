$(function() {
    console.log("READY ...");
});


function openModal(modal_title, form_id, post) {
    let modal = $('#postModal');
    modal.find('#postModalLabel').text(modal_title);

    let form = modal.find('form');
    form.prop('id', form_id);
    form[0].reset();

    if (post) {    
        // Se edita un post
        form.find('#title_input').val(post.title);
        form.find('#description_input').val(post.description);
        form.data('post_id', post.id);        
    }

    modal.modal('show');
}

$(document).on('click', '#new_post_btn', function(e) {
    openModal('Nuevo Post', 'new_post_form', null);
});


function createToastAlert(toastClass, msg) {
    // text-white bg-primary
    let alert = `
        <div class="toast align-items-center ${toastClass} show" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="true" data-bs-delay="4000">
            <div class="d-flex">
                <div class="toast-body"> ${msg}                
            </div>
                <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
        </div>
    `;

    $('div.toast-container').append(alert);
}


function show_error_alert(jqXHR) {
    if (typeof jqXHR.responseJSON == "object") {
        let errors = jqXHR.responseJSON;
        
        let errors_list = `<ul>`;
        for (let err in errors) errors_list += `<li>${ errors[err] }</li>`;
        errors_list += `</ul>`;

        createToastAlert('text-white bg-danger', errors_list);
    }
    else createToastAlert('text-white bg-danger',jqXHR.responseJSON);
}

function sendData(form, url, isNew, msg) {
    let data = new FormData(form[0]);

    $.ajax({
        type: "POST", url, data,
        contentType: false, processData: false,
        success: function(response) {            
            createToastAlert('text-white bg-success', msg);
            if (isNew) addRow(response);
            else setTimeout(() => window.location.href = `/posts/${response.slug}`, 3500);            

            $('#postModal').modal('hide');
        }
    })
    .fail(jqXHR => show_error_alert(jqXHR));
}


function addRow(post) {
    post.created = moment(post.created_at).format('DD/MM/YYYY HH:mm');

    let row =
        `<tr>
            <td>${post.title}</td>
            <td>${post.created}</td>
            <td>${post.description.substring(0, 20)} ...</td>
            <td>
                <a href="/posts/${post.slug}" class="btn btn-outline-secondary" role="button">Ver</a>
                <button type="button" class="btn btn-outline-danger delete_btn" data-post_id="${post.id}">Borrar</button>
            </td>
        <tr>`;
    
    $('tbody').append(row);
}


$(document).on('submit', '#new_post_form', function(e) {
    e.preventDefault();
    
    let form = $(this);
    sendData(form, '/posts', true, 'Post creado correctamente!');
});


// ---------- Edicion de post ---------------------
$(document).on('click', '#edit_btn', function(e) {
    let post = $(this).data('post');
    openModal('Editar Post', 'edit_post_form', post);
});

$(document).on('submit', '#edit_post_form', function(e) {
    e.preventDefault();
    
    let form = $(this);
    let post_id = form.data('post_id');
    sendData(form, `/posts/${post_id}?_method=PUT`, false, 'Post actualizado correctamente!');
});


// ---------- Borrar de post ---------------------
$(document).on('click', '#delete_btn, .delete_btn', function(e) {
    let btn = $(this);
    let post_id = btn.data('post_id');
    let _token = $('input[name="_token"]').val();

    let response = window.confirm('¿Está seguro de eliminar?');
    if (response) {
        $.ajax({
            type: "POST", data: {_token}, 
            url: `/posts/${post_id}?_method=DELETE`,        
            success: function(data) {
                createToastAlert('text-white bg-success', 'Post eliminado correctamente!');           
                if (btn.hasClass('delete_btn')) btn.closest('tr').remove(); // Se elimina desde la table
                else setTimeout(() => window.location.href = `/posts`, 3500); // Se elimina desde la vista del post           
            }
        })
        .fail(jqXHR => show_error_alert(jqXHR));
    }    
});


// ---------- Filtrar posts ---------------------
$(document).on('submit', '#filter_posts_form', function(e) {
    e.preventDefault();
    
    let form = $(this);
    let data = form.serialize();

    $.ajax({
        type: "GET", data, 
        url: `/posts_filter`,        
        success: function(posts) {
            let tbody = $('tbody');
            tbody.empty();

            if (posts.length) posts.forEach(post => addRow(post))            
            else {
                let row = '<tr><td colspan="4" style="text-align: center;"> No hay resultados ...</td></tr>';                
                tbody.append(row)
            }            
        }
    })
    .fail(jqXHR => show_error_alert(jqXHR));
});
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Posts routes 
Route::get('posts', 'PostController@index');
Route::post('posts', 'PostController@store');
Route::get('posts/{slug}', 'PostController@show');
Route::put('posts/{post}', 'PostController@update');
Route::delete('posts/{post}', 'PostController@destroy');
Route::get('posts_filter', 'PostController@filter');

// Contacts routes
Route::get('contacts', 'ContactController@index');
Route::post('contacts/send_email', 'ContactController@send_email');